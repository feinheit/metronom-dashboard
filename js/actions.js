import {getApiEndpoint} from './api'

export async function fetchData(dispatch, getState) {
  const {settings} = getState()

  if (!settings) return

  try {
    const jobsResponse = await fetch(getApiEndpoint('jobs', settings), {
      credentials: 'same-origin',
      mode: 'cors',
    })

    const jobsData = await jobsResponse.json()

    dispatch({
      type: 'JOBS_LOADED',
      jobs: jobsData,
    })
  } catch (err) {
    window.console.error(err)
    dispatch(
      createMessage(
        'Jobs und Activities konnten nicht geladen werden. Ist die Metronom URL und der API Key korrekt eingetragen?',
        'error'
      )
    )
  }

  try {
    const activitiesResponse = await fetch(
      getApiEndpoint('activities', settings),
      {
        credentials: 'same-origin',
        mode: 'cors',
      }
    )

    const activitiesData = await activitiesResponse.json()

    dispatch({
      type: 'ACTIVITIES_LOADED',
      activities: activitiesData,
    })
  } catch (err) {
    // dispatch(errorMessageAction())
    // dispatch({type: 'SESSION_FAILED', error: err})
    window.console.error(err)
  }
}

export function createMessage(text, type) {
  return {
    type: 'CREATE_MESSAGE',
    messageText: text,
    messageType: type,
  }
}
