import React, {Component} from 'react'
import {connect} from 'react-redux'

import {createMessage} from './actions'
import {getApiEndpoint} from './api'

const SECOND = 1000
const MINUTE = 60 * SECOND
const HOUR = 60 * MINUTE

class Timer extends Component {
  constructor(props) {
    super(props)

    this.interval = null

    this.state = {
      time: '00:00:00',
      decimalHours: '0.0',
      submitting: false,
    }
  }

  toggleRunning = e => {
    e.preventDefault()

    this.props.timer.running
      ? this.props.pauseTimer(this.props.timer.id)
      : this.props.startTimer(this.props.timer.id)
  }

  remove = e => {
    e.preventDefault()
    this.props.removeTimer(this.props.timer.id)
  }

  submit = async e => {
    e.preventDefault()

    if (this.state.submitting) return

    this.setState({
      submitting: true,
    })

    this.props.dispatch({
      type: 'SUBMIT_TIMER_STARTED',
      timerId: this.props.timer.id,
    })

    try {
      const res = await fetch(
        getApiEndpoint('create', this.props.settings) + this.getQueryString(),
        {
          credentials: 'same-origin',
          mode: 'cors',
        }
      )

      if (res.status === 400) {
        const responseText = await res.text()
        throw Error(responseText)
      }

      this.setState({
        submitting: false,
      })
      this.props.dispatch({
        type: 'SUBMIT_TIMER_SUCCESS',
        timerId: this.props.timer.id,
        jobId: this.getJobId(),
        activityId: this.getActivityId(),
      })
    } catch (err) {
      this.props.dispatch({
        type: 'SUBMIT_TIMER_FAILED',
        timerId: this.props.timer.id,
      })

      this.props.dispatch(createMessage(err.toString(), 'error'))

      this.setState({
        submitting: false,
      })
      console.error(err)
    }
  }

  getQueryString() {
    return (
      '&job=' +
      this.getJobId() +
      '&activity=' +
      this.getActivityId() +
      '&notes=' +
      this.notesField.value +
      '&hours=' +
      encodeURIComponent(this.state.decimalHours)
    )
  }

  getActivityId() {
    return this.activitiesField.select.current.value
  }

  getJobId() {
    return this.jobsField.select.current.value
  }

  getLastJobActivity(job) {
    let jobActivity = this.props.lastJobActivities.find(
      jobActivity => jobActivity.jobId === job
    )

    if (jobActivity) {
      return jobActivity.activityId
    } else {
      return null
    }
  }

  updateTime() {
    if (!this.props.timer.startedAt) return

    const startedAt = new Date(this.props.timer.startedAt)
    let delta

    if (this.props.timer.pausedAt && !this.props.timer.running) {
      delta = new Date(this.props.timer.pausedAt) - startedAt
    } else {
      delta = new Date() - startedAt
    }

    const passedHours = Math.floor(delta / HOUR)
    const passedMinutes = Math.floor(delta / MINUTE) % 60
    const passedSeconds = Math.floor(delta / SECOND) % 60

    const formatedTime = `${this.twoDigit(passedHours)}:${this.twoDigit(
      passedMinutes
    )}:${this.twoDigit(passedSeconds)}`
    const decimalHours = Math.ceil(delta / HOUR * 10) / 10

    this.setState({
      time: formatedTime,
      decimalHours: decimalHours,
    })
  }

  twoDigit(number) {
    if (number < 10) return '0' + number
    return number
  }

  componentDidMount() {
    this.updateTime()

    if (this.props.timer.running) {
      this.interval = setInterval(() => {
        this.updateTime()
      }, 1000)
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.timer.running != this.props.timer.running) {
      if (this.props.timer.running) {
        this.interval = setInterval(() => {
          this.updateTime()
        }, 1000)
      } else {
        clearInterval(this.interval)
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  updateFields = () => {
    let jobId = this.getJobId()
    let activityId = this.getActivityId()

    if (jobId && !activityId) {
      activityId = this.getLastJobActivity(jobId)
    }

    this.props.updateTimer(this.props.timer.id, {
      activity: activityId,
      job: jobId,
      notes: this.notesField.value,
    })
  }

  render() {
    const {time, decimalHours} = this.state
    const {running, notes, activity, job} = this.props.timer

    return (
      <div className={`timer ${running ? 'timer--running' : ''}`}>
        <div className="timer__time-wrapper">
          <time className="timer__decimal-hours">{decimalHours}</time>
          <time className="timer__time">{time}</time>
        </div>
        <label>Job</label>
        <Select
          list={this.props.jobs}
          ref={ref => (this.jobsField = ref)}
          onChange={this.updateFields}
          value={job}
        />
        <label>Activity</label>
        <Select
          list={this.props.activities}
          ref={ref => (this.activitiesField = ref)}
          onChange={this.updateFields}
          value={activity}
        />
        <label>Note</label>
        <textarea
          ref={ref => (this.notesField = ref)}
          onChange={this.updateFields}
          value={notes}
        />
        <div className="timer__buttons">
          <button className="button" onClick={this.toggleRunning}>
            {running ? 'Pause' : 'Start'}
          </button>
          <button className="button" onClick={this.submit}>
            Send
          </button>
        </div>
        <button className="timer__remove" onClick={this.remove}>
          ×
        </button>
      </div>
    )
  }
}

export default connect(
  state => ({
    jobs: state.jobs,
    activities: state.activities,
    settings: state.settings,
    lastJobActivities: state.lastJobActivities,
  }),
  dispatch => ({
    removeTimer: id =>
      dispatch({
        type: 'REMOVE_TIMER',
        timerId: id,
      }),
    startTimer: id =>
      dispatch({
        type: 'START_TIMER',
        timerId: id,
      }),
    pauseTimer: id =>
      dispatch({
        type: 'PAUSE_TIMER',
        timerId: id,
      }),
    updateTimer: (id, update) =>
      dispatch({
        type: 'UPDATE_TIMER',
        timerId: id,
        timer: update,
      }),
    dispatch,
  })
)(Timer)

class Select extends Component {
  constructor(props) {
    super(props)
    this.select = React.createRef()
  }

  render() {
    const {list, ...props} = this.props
    const options = list.map((item, i) => (
      <option key={i} value={item[1]}>
        {item[0]}
      </option>
    ))

    return (
      <select ref={this.select} {...props}>
        {options}
      </select>
    )
  }
}
