import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import ReduxThunk from 'redux-thunk'
import persistState from 'redux-localstorage'

import reducers from './reducers'

const middleware = [ReduxThunk]

if (process.env.NODE_ENV !== 'production') {
  middleware.push(require('redux-logger').default)
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  combineReducers({
    ...reducers,
  }),
  undefined, // window.__initial_context,
  composeEnhancers(persistState(), applyMiddleware(...middleware))
)

store.dispatch({type: 'INIT'})

export default store
