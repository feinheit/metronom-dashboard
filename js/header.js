import React from 'react'
import {connect} from 'react-redux'

const Header = ({dispatch}) => {
  const add = e => {
    e.preventDefault()
    dispatch({type: 'ADD_TIMER'})
  }

  const openSettings = e => {
    e.preventDefault()
    dispatch({type: 'OPEN_SETTINGS'})
  }

  return (
    <header className="header">
      <h1 className="header__title">Metronom Dashboard</h1>
      <button className="header__button" onClick={add}>
        Add
      </button>
      <button className="header__button" onClick={openSettings}>
        Settings
      </button>
      <button className="header__button header__button--reload">↻</button>
    </header>
  )
}

export default connect()(Header)
