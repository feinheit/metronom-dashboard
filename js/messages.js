import React from 'react'
import {connect} from 'react-redux'

const Messages = ({messages, removeMessage}) => {
  const messageElements = messages.map(message => (
    <li key={message.id} className={`message message--${message.type}`}>
      {message.text}
      <button
        className="message__close"
        onClick={() => removeMessage(message.id)}
      >
        ×
      </button>
    </li>
  ))

  return messageElements.length ? (
    <ul className="messages">{messageElements}</ul>
  ) : null
}

export default connect(
  state => ({
    messages: state.messages,
  }),
  dispatch => ({
    removeMessage: id =>
      dispatch({
        type: 'REMOVE_MESSAGE',
        messageId: id,
      }),
  })
)(Messages)
