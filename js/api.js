const API = {
  jobs: 'api/job_list/',
  activities: 'api/activity_list/',
  create: 'api/worklog_create/',
}

export function getApiEndpoint(endpoint, settings) {
  return settings.baseUrl + API[endpoint] + '?apikey=' + settings.apiKey
}
