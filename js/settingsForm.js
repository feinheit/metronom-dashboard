import React, {Component} from 'react'
import {connect} from 'react-redux'

class SettingsForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      valid: false,
    }
  }

  checkValidity = () => {
    this.setState({
      valid:
        this.baseUrlField.checkValidity() && this.apiKeyField.checkValidity(),
    })
  }

  save = e => {
    e.preventDefault()

    if (!this.state.valid) return

    this.props.saveSettings({
      baseUrl: this.baseUrlField.value,
      apiKey: this.apiKeyField.value,
    })
  }

  render() {
    const {valid} = this.state
    const {settings} = this.props

    return (
      <div className="settings">
        <form className="settings__form" onSubmit={this.save}>
          <h1>Settings</h1>
          <label htmlFor="id_base-url">Base URL</label>
          <input
            type="text"
            id="id_base-url"
            name="base-url"
            ref={ref => (this.baseUrlField = ref)}
            onInput={this.checkValidity}
            defaultValue={settings.baseUrl || 'https://metronom.feinheit.ch/'}
            required
          />
          <label htmlFor="id_api-key">API Key</label>
          <input
            type="text"
            id="id_api-key"
            name="api-key"
            ref={ref => (this.apiKeyField = ref)}
            onInput={this.checkValidity}
            defaultValue={settings.apiKey}
            required
          />
          <button type="submit" className="button" disabled={!valid}>
            Save
          </button>
        </form>
      </div>
    )
  }
}

export default connect(
  state => ({
    settings: state.settings,
  }),
  dispatch => ({
    saveSettings: settings =>
      dispatch({
        type: 'UPDATE_SETTINGS',
        settings,
      }),
  })
)(SettingsForm)
