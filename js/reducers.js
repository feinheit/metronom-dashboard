function jobs(state = [], action) {
  switch (action.type) {
    case 'JOBS_LOADED':
      return action.jobs
    default:
      return state
  }
}

function activities(state = [], action) {
  switch (action.type) {
    case 'ACTIVITIES_LOADED':
      return action.activities
    default:
      return state
  }
}

function settings(state = {}, action) {
  switch (action.type) {
    case 'UPDATE_SETTINGS':
      return action.settings
    default:
      return state
  }
}

function showSettings(state = false, action) {
  switch (action.type) {
    case 'CLOSE_SETTINGS':
    case 'UPDATE_SETTINGS':
      return false
    case 'OPEN_SETTINGS':
      return true
    default:
      return state
  }
}

function timers(state = [], action) {
  switch (action.type) {
    case 'REMOVE_TIMER':
      return state.filter(timer => timer.id !== action.timerId)
    case 'UPDATE_TIMER':
      return state.reduce((acc, currentTimer) => {
        if (currentTimer.id === action.timerId) {
          acc.push({...currentTimer, ...action.timer})
        } else {
          acc.push(currentTimer)
        }
        return acc
      }, [])
    case 'START_TIMER':
      return state.reduce((acc, currentTimer) => {
        if (currentTimer.id === action.timerId) {
          acc.push(_startTimer(currentTimer))
        } else if (currentTimer.running) {
          acc.push(_pauseTimer(currentTimer))
        } else {
          acc.push(currentTimer)
        }
        return acc
      }, [])
    case 'PAUSE_TIMER':
      return state.reduce((acc, currentTimer) => {
        if (currentTimer.id === action.timerId) {
          acc.push(_pauseTimer(currentTimer))
        } else {
          acc.push(currentTimer)
        }
        return acc
      }, [])
    case 'ADD_TIMER':
      return state.concat([_createTimer()])
    case 'SUBMIT_TIMER_SUCCESS':
      return state.reduce((acc, currentTimer) => {
        if (currentTimer.id === action.timerId) {
          acc.push(_createTimer())
        } else {
          acc.push(currentTimer)
        }
        return acc
      }, [])
    default:
      return state
  }
}

function _createTimer() {
  return {
    id: Math.random(),
    time: 0,
    notes: '',
    job: '',
    activity: '',
    startedAt: null,
    pausedAt: null,
  }
}

function _startTimer(timer) {
  let startTime = new Date().getTime()

  if (timer.pausedAt) {
    const delta = timer.pausedAt - timer.startedAt
    startTime = startTime - delta
  }

  return {
    ...timer,
    running: true,
    startedAt: startTime,
    pausedAt: null,
  }
}

function _pauseTimer(timer) {
  return {
    ...timer,
    running: false,
    pausedAt: new Date().getTime(),
  }
}

function lastJobActivities(state = [], action) {
  switch (action.type) {
    case 'SUBMIT_TIMER_SUCCESS':
      return _addJobActivity(state, {
        activityId: action.activityId,
        jobId: action.jobId,
      })
    default:
      return state
  }
}

function _addJobActivity(list, newJobActivity) {
  let jobActivity = list.find(item => item.jobId === newJobActivity.jobId)

  if (jobActivity) {
    return list.reduce((acc, entry) => {
      if (entry.jobId === newJobActivity.jobId) {
        acc.push(newJobActivity)
      } else {
        acc.push(entry)
      }

      return acc
    }, [])
  } else {
    list.push(newJobActivity)
    return list
  }
}

function messages(state = [], action) {
  switch (action.type) {
    case 'INIT':
      return []
    case 'CREATE_MESSAGE':
      return state.concat([
        _createMessage(action.messageText, action.messageType),
      ])
    case 'REMOVE_MESSAGE':
      return state.filter(message => message.id !== action.messageId)
    default:
      return state
  }
}

function _createMessage(text = '', type = 'info') {
  return {
    id: Math.random(),
    text: text,
    type: type,
  }
}

export default {
  jobs,
  activities,
  settings,
  showSettings,
  timers,
  lastJobActivities,
  messages,
}
