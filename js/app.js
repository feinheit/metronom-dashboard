import React from 'react'
import ReactDOM from 'react-dom'
import {Provider, connect} from 'react-redux'

import {fetchData} from './actions'
import {completeSettings, showSettings} from './selectors'

import store from './store'
import Header from './header'
import SettingsForm from './settingsForm'
import Timer from './timer'
import Messages from './messages'

const TimersComponent = ({timers}) => {
  const timerList = timers.map(timer => <Timer key={timer.id} timer={timer} />)

  return <div className="timers">{timerList}</div>
}

const Timers = connect(state => ({
  timers: state.timers,
}))(TimersComponent)

const AppComponent = ({dispatch, completeSettings, showSettings}) => {
  if (completeSettings) {
    dispatch(fetchData)
  }

  return (
    <div className="content">
      <Header />
      <Messages />
      <Timers />
      {showSettings ? <SettingsForm /> : null}
    </div>
  )
}

const App = connect(state => ({
  completeSettings: completeSettings(state),
  showSettings: showSettings(state),
}))(AppComponent)

export function renderInElement(element) {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    element
  )
}
