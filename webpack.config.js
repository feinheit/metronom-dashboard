/* global __dirname, process */
var path = require('path')
var webpack = require('webpack')
var MiniCssExtractPlugin = require('mini-css-extract-plugin')
var CleanWebpackPlugin = require('clean-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')

// var HOST = process.env.HOST || '127.0.0.1'
var DEBUG = process.env.NODE_ENV !== 'production'
var HTTPS = !!process.env.HTTPS

function cssLoader(firstLoader) {
  return [
    DEBUG ? 'style-loader?sourceMap' : MiniCssExtractPlugin.loader,
    'css-loader?sourceMap',
    'postcss-loader?sourceMap',
  ]
    .concat(firstLoader || [])
    .filter(function(el) {
      return !!el
    })
}

module.exports = {
  mode: DEBUG ? 'development' : 'production',
  context: __dirname,
  devtool: 'source-map',
  entry: {
    main: './main.js',
  },
  output: {
    path: path.resolve('./build/'),
    // publicPath: DEBUG
    //   ? 'http' + (HTTPS ? 's' : '') + '://' + HOST + ':8000/'
    //   : '',
    publicPath: '/',
    // [chunkhash] cannot be used with HMR.
    filename: DEBUG ? '[name].js' : '[name]-[chunkhash].js',
    chunkFilename: DEBUG ? '[name].js' : '[name]-[chunkhash].js',
  },
  // recordsPath: path.resolve('./records.json'),  // TODO necessary?
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  'env',
                  {
                    modules: false,
                    // debug: true,
                    targets: {
                      // browsers: null,
                      // browsers: packageJson.browserslist,
                    },
                  },
                ],
                'react',
              ],
              plugins: [
                'transform-object-rest-spread',
                'transform-class-properties',
              ],
              cacheDirectory: path.resolve(__dirname, 'tmp'),
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: cssLoader(),
      },
      {
        test: /\.scss$/,
        use: cssLoader({
          loader: 'sass-loader',
          options: {
            includePaths: [path.resolve(path.join(__dirname, 'node_modules'))],
            outputStyle: 'expanded',
            sourceMap: true,
          },
        }),
      },
      {
        test: /\.less$/,
        use: cssLoader('less-loader'),
      },
      {
        test: /\.(png|woff|woff2|svg|eot|ttf|gif|jpe?g)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1000,
              // ManifestStaticFilesStorage reuse.
              name: '[path][name].[md5:hash:hex:12].[ext]',
              // No need to emit files in production, collectstatic does it.
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules'],
    alias: {},
  },
  plugins: [
    DEBUG ? null : new CleanWebpackPlugin([path.resolve('./build/')]),
    DEBUG
      ? null
      : new MiniCssExtractPlugin({
          filename: '[name]-[contenthash].css',
        }),
    DEBUG
      ? new webpack.NamedModulesPlugin()
      : new webpack.HashedModuleIdsPlugin(),
    /* Pull all CSS and node_modules mods into the common bundle
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      minChunks: ({resource}) => /(node_modules|css)/.test(resource),
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['common', 'manifest'],
    }),
    */
    new HtmlWebpackPlugin({
      title: 'Metronom Dashboard',
      meta: {viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'},
      template: 'dashboard.html',
      filename: 'dashboard.html',
      chunks: ['main'],
      inject: true,
    }),
  ].filter(function(el) {
    return !!el
  }),
  devServer: {
    contentBase: false,
    inline: true,
    quiet: false,
    https: HTTPS,
    disableHostCheck: true,
    headers: {'Access-Control-Allow-Origin': '*'},
    historyApiFallback: true, // fallback on all routes to /index.html
    before: function(app) {
      function answer(req, res) {
        console.log(req.method, req.headers['content-type'], req.body) // eslint-disable-line
        res.json({success: true})
      }

      var express = require('express')
      app.use(require('body-parser').text({type: '*/*'}))
      app.post('/data/answer/', answer)
      app.put('/data/answer/', answer)
      app.use('/data', express.static(path.resolve('./data/')))
    },
  },
  performance: {
    // No point warning in development, since HMR / CSS bundling blows up
    // the asset / entrypoint size anyway.
    hints: DEBUG ? false : 'warning',
  },
}
