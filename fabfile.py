from fabric.api import env, local, task
from fabric.contrib.project import rsync_project


env.hosts = ['deploy@metronom.feinheit.ch']


@task
def deploy():
    local('yarn run prod')
    rsync_project(
        local_dir='build/',
        remote_dir='www/metronom/htdocs/',
        delete=True,
    )
    rsync_project(
        local_dir='build/',
        remote_dir='www/bf-metronom/htdocs/',
        delete=True,
    )
